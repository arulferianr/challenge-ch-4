// DEFINE AND GET ELEMENT
// get element untuk player
p1rock = document.getElementById("player-rock");
p1paper = document.getElementById("player-paper");
p1scissor = document.getElementById("player-scissor");
// get element untuk com
c1rock = document.getElementById("com-rock");
c1paper = document.getElementById("com-paper");
c1scissor = document.getElementById("com-scissor");
// get element selection and all choice rock or paper or scissor(rps)
vs = document.getElementById("vs");
allChoice = document.getElementsByClassName("choice");

// create class game 
class Game {
  // create object rps/all choices
  choice = {
    p1rock: p1rock,
    p1paper: p1paper,
    p1scissor: p1scissor,
    c1rock: c1rock,
    c1paper: c1paper,
    c1scissor: c1scissor,
  };
  // create method to play game, define result for game and change UI/Background
  play(Player1, Com1, inputPlayer1) {
    this.changeUI(Player1,Com1);

    let result = this.getResult(Player1,Com1);
    this.logResult(result);
  }
  // code for change UI/Background when player and com play the game
  changeUI(Player1, Com1) {
    let p1Choice = Player1.getChoice(); // get data player choice
    let c1Choice = Com1.getChoice(); 
    
    this.setRPSBGColor("p1" + p1Choice); //set UI based on player choice
    this.setRPSBorderRadius("p1" +p1Choice);

    this.setRPSBGColor("c1" + c1Choice); //set UI based on computer choice
    this.setRPSBorderRadius("c1" +c1Choice);

    this.setRPSAttributeDisabled();
  }

  setRPSBGColor(CHOICE) {
    this.choice[CHOICE].style.backgroundColor = '#C4C4C4';
  }

  setRPSBorderRadius(CHOICE) {
    this.choice[CHOICE].style.borderRadius = "1rem";
  }

  setRPSAttributeDisabled() {
    for (const element of allChoice) {
      element.setAttribute("disabled", "disabled");
    }
  }

  setVSUIResult() {
    vs.style.position = "relative";
    vs.style.top = "30%";
    vs.style.left = "10%";
    // vs.style.margin = "0";
    vs.style.justifyContent = "center";
    vs.style.fontSize = "25px";
    vs.style.color = "white";
    vs.style.backgroundColor = "#4C9654";
    vs.style.transform = "rotate(-28deg)";
    vs.style.width = "150px";
    vs.style.borderRadius = "0.5rem";
    vs.style.padding = "10px";
    vs.style.textAlign = "center";
  }

  getResult(Player1, Com1) {
    let playerName = Player1.getName();
    let comName = Com1.getName();
    let playerChoice = Player1.getChoice();
    let comChoice = Com1.getChoice();

    if(playerChoice == comChoice) {
      vs.innerHTML = "DRAW";
      this.setVSUIResult();
      vs.style.backgroundColor = "green";
      return 'DRAW';
    } else if (playerChoice == "rock") {
      if (comChoice == "paper") {
        vs.innerHTML = `${comName} <br> WIN`;
        this.setVSUIResult();
        return `${comName} WIN`;
      } else {
        vs.innerHTML = `${playerName} <br> WIN`;
        this.setVSUIResult();
        return `${playerName} WIN`;
      }
    } else if (playerChoice == "paper") {
      if (comChoice == "scissor") {
        vs.innerHTML = `${comName} <br> WIN`;
        this.setVSUIResult();
        return `${comName} WIN`;
      } else {
        vs.innerHTML = `${playerName} <br> WIN`;
        this.setVSUIResult();
        return `${playerName} WIN`;
      }
    } else if (playerChoice == "scissor") {
      if (comChoice == "rock") {
        vs.innerHTML = `${comName} <br> WIN`;
        this.setVSUIResult();
        return `${comName} WIN`;
      } else {
        vs.innerHTML = `${playerName} <br> WIN`;
        this.setVSUIResult();
        return `${playerName} WIN`;
      }
    }
  }

  logResult(result) {
    console.log(result);
  }

}

class Player {
  option = ["rock", "paper", "scissor"];
  constructor(name, choose = 0) {
    if (this.constructor === Player) {
      throw new Error("Cannot instantiate from Abstract Class");
    }
    this.name = name;
    this.choose = choose;
  }
}

class HumanPlayer extends Player {
  constructor(name, choose = 0) {
    super(name,choose);
  }
  getName() {
    return this.name;
  }
  setChoice(choose) {
    this.choose =choose;
  }
  getChoice() {
    return this.choose;
  }
}

class ComputerPlayer extends Player {
  constructor(name, choose = 0) {
    super(name,choose);
    this.choose = this.setChoice();
  }
  getName() {
    return this.name;
  }
  setChoice() {
    return this.option[Math.floor(Math.random() * this.option.length)];
  }
  getChoice() {
    return this.choose;
  }
}

function start(input) {
  const game = new Game();

  human1 = new HumanPlayer("PLAYER 1", input);
  com = new ComputerPlayer("COM");

  game.play(human1, com, input);
}
